Ezy
===

Ezy is an alternative background manager for Gnome.
Tested on Gnome 3 and Gnome 3 fallback.

Version 0.2a
===

[+] Added the -r(andom) option. Now Ezy can change your wallpaper randomly every hour.
Wallpapers are taken from your ~/Pictures folder.

[+] Fixed a problem with blank spaces in the files name.